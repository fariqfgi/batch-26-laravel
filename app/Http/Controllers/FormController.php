<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index()
    {
        return view('form');
    }

    public function kirim(Request $request)
    {
        $fname = $request['fname'];
        $lname = $request['lname'];

        return view('welcome', compact('fname', 'lname'));
    }
}
