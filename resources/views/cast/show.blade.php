@extends('layouts.master')

@section('title')
    Cast Data by id
@endsection

@section('content')
    <p>Nama: {{ $cast->nama }}</p>
    <p>Umur: {{ $cast->umur }} tahun</p>
    <p>Bio: {{ $cast->bio }}</p>
@endsection