<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook | Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/kirim" method="POST">
        @csrf
        <label for="fname">First Name:</label><br>
        <input type="text" name="fname" id="fname"><br>
        <br>
        <label for="lname">Last Name:</label><br>
        <input type="text" name="lname" id="lname"><br>
        <br>
        <label for="lgender">Gender: </label><br>
        <input type="radio" name="gender" id="male" value="male"> Male <br>
        <input type="radio" name="gender" id="female" value="female"> Female <br>
        <input type="radio" name="gender" id="other" value="other"> Other <br>
        <br>
        <label for="lnationality">Nationality:</label><br>
        <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="other">Other</option>
        </select>
        <br><br>
        <label for="llanguage">Language Spoken:</label><br>
        <input type="checkbox" name="language" id="indonesia" value="indonesia">
        <label for="indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" name="language" id="english" value="english">
        <label for="english">English</label><br>
        <input type="checkbox" name="language" id="other" value="other">
        <label for="other">Other</label><br>
        <br>
        <label for="lbio">Bio:</label><br>

        <textarea name="bio" id="bio" cols="30" rows="8"></textarea><br>
        <button type="submit">Sign Up</button>
    </form>
</body>
</html>