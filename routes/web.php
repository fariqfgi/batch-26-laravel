<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/form', 'FormController@index');
Route::post('/kirim', 'FormController@kirim');
Route::get('/welcome', 'WelcomeController@index');
Route::get('/master', function () {
    return view('layouts.master');
});
Route::get('/data-table', function () {
    return view('datatable');
});
Route::get('/table', function () {
    return view('table');
});

// CRUD cast
Route::get('/cast/create', 'CastController@create');
Route::get('/cast', 'CastController@index');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
